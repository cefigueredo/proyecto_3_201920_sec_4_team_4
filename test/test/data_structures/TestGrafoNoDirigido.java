package test.data_structures;

import static org.junit.Assert.*;

import org.junit.*;

import model.data_structures.GrafoNoDirigido;
import model.data_structures.Vertex;

public class TestGrafoNoDirigido <K, V>{

	GrafoNoDirigido<K, V> g;
	private final static int S = 1000;
	@SuppressWarnings("unchecked")
	@Before
	public void setUp1() {
		g = new GrafoNoDirigido<K, V>();
		g.Graph(S);
		for(int i = 0; i < S; i=i+2) {

			Object s = i;
			Object z = i+1;
			
			g.addVertex((int)s, z.toString());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetInfoVertex() {
		Object s = S-1;
		Object z = S-2;
		g.setInfoVertex((K)s, (V)z);
		assertEquals(z,  g.getInfoVertex( (int)s )  );
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getCostArc() {
		Object s = 0;
		Object z = 1;
		double cost = 3.0;
		double nocost = -1.0;
		g.addEdge((int)s, (int)z, cost, cost, cost);
		assertEquals(nocost, g.getCostArc((int)z, (int)s)[0], 0);
	}
	
	@Test
	public void testUncheck() {
		assertEquals(1,1);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetCC() {
		Object s = 0;
		Object z = 1;
		double cost = 3.0;
		g.addVertex((int)s, s.toString() );
		g.addVertex((int)z, z.toString());
		g.addEdge((int)s, (int)z, cost, cost, cost);
		g.cc();
		boolean b = false;
		for(K m : g.getCC((K) s)) {
			if(m == z) {
				b = true;
			}
		}
		assertTrue(b);
	}
}
