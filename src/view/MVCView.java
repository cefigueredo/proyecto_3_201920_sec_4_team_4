package view;

import model.logic.MVCModelo;

public class MVCView 
{
	    /**
	     * Metodo constructor
	     */
	    public MVCView()
	    {
	    	
	    }
	    
		public void printMenu()
		{
			System.out.println("1. Cargar datos");
			System.out.println("2. Generar JSON");
			System.out.println("3. Buscar camino mas corto - 1A");
			System.out.println("4. Cargar grafo persistido");
			System.out.println("5. Dar cantidad de componentes conectados");
			System.out.println("6. Graficar");
			System.out.println("7. Dar N Vertices con menor velocidad - 2A");
			System.out.println("8. PrimMST CC mas grande -3A");
			System.out.println("9. Encontrar menor distancia en Haversine - 1B");
			System.out.println("10. Dar vertices alcanzables en un tiempo - 2B");
			System.out.println("11. Calcular MST con distancia - 3B");
			System.out.println("12. Construir grafo - 1C");
			System.out.println("13. Calcular camino de costo m�nimo - 2C");
			System.out.println("14. Calcular caminos menos arcos en todas las zonas alcanzables - 3C");
			System.out.println("15. Exit");
			System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
		}

		public void printMessage(String mensaje) {

			System.out.println(mensaje);
		}		
		
		public void printModelo(MVCModelo modelo)
		{
			
		}
}
