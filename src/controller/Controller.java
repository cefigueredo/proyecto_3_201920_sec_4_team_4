package controller;

import java.util.Scanner;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() throws Exception 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					
				    
				    modelo = new MVCModelo(); 
					modelo.cargarDatos();					
					break;

				case 2:
					System.out.println("Se guardar� en './data/persistencia.json'");
					modelo.generarJSON();
					
					System.out.println("JSON generado exitosamente.");						
					break;

				case 3:
					System.out.println("Ingrese la longitud de origen: ");
					double long1;
					long1 = lector.nextDouble();
					System.out.println("Ingrese la latitud de origen: ");
					double lat1;
					lat1 = lector.nextDouble();
					System.out.println("Ingrese la longitud de destino: ");
					double long2;
					long2 = lector.nextDouble();
					System.out.println("Ingrese la latitud de destino: ");
					double lat2;
					lat2 = lector.nextDouble();
					modelo.encontrarCaminoCostoMinimo(lat1, long1, lat2, long2);			
					break;
				case 4:
					
					System.out.println("Cargando grafo persistido.");
					modelo.cargarGrafoPersistido();
					break;
					
				case 5:
					System.out.println("Calculando cantidad total de componentes conectados.");
					modelo.darCantidadCC();
					break;
					
				case 6:
					System.out.println("Graficando.");
					
					break;
					
				case 7:
					System.out.println("N-numero de vertices a mostrar");
					int n = lector.nextInt();
					modelo.verticesMenorVel(n);	
					break;

				case 8: 
					modelo.MSTPrim();						
					break;	
				case 9:
					System.out.println("Ingrese latitud de or�gen: ");
					double a = lector.nextDouble();
					System.out.println("Ingrese longitud de or�gen: ");
					double a2 = lector.nextDouble();
					System.out.println("Ingrese latitud de destino: ");
					double b = lector.nextDouble();
					System.out.println("Ingrese longitud de destino: ");
					double b2 = lector.nextDouble();
					modelo.encontrarMenorHaversine(a, a2, b, b2);
					break;
				case 10:
					System.out.println("Ingrese latitud de or�gen: ");
					double c = lector.nextDouble();
					System.out.println("ingrese longitud de or�gen: ");
					double c2 = lector.nextDouble();
					System.out.println("Ingrese tiempo m�ximo: ");
					int t = lector.nextInt();
					modelo.darVerticesAlcanzables(c, c2, t);
					
					break;
				case 11:
					System.out.println("Calculando...");
					modelo.calcularMSTConKruskal();
					break;
				case 12:
					System.out.println();
					modelo.construirGrafoSimplificado();
					break;
				case 13:
					System.out.println();
					modelo.calcularCaminoCostoMinimo();
					break;
				case 14:
					System.out.println();
					modelo.calcularMenorLongitudZonaAlcanzable();
					break;
				case 15: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
