package model.data_structures;

import java.util.ArrayList;

public class SP{
	private int[] edgeTo;
	private double distTo[];
	private BinaryHeapPQ<Integer,Double> pq;
	public SP(GrafoNoDirigido G, int s)
	{
		edgeTo= new int[G.V()];
		distTo= new double[G.V()];
		pq= new BinaryHeapPQ<>(G.V());
		for(int v=0;v<G.V();v++) distTo[v]=Double.POSITIVE_INFINITY;
		distTo[s] = 0;
		pq.insert(s, 0.0);
		while(!pq.isEmpty())
		{
			relax(G,(int)pq.delMin());
		}
	}
	private void relax(GrafoNoDirigido G, int v)
	{
		ArrayList ad=(ArrayList) G.adjNode(v);
		for(int i=0;i< ad.size();i++)
		{
			Node inf=(Node) ad.get(i);
			int w = inf.item();
			if(distTo[w]>distTo[v]+inf.value[1])
			{
				distTo[w]=distTo[v]+inf.value[1];
				edgeTo[w]=v;
				if(pq.contains(w)) pq.change(w,distTo[w]);
				else pq.insert(w, distTo[w]);
			}
		}
	}
	public int[] getEdgeTo() {
		return edgeTo;
	}
	public void setEdgeTo(int[] edgeTo) {
		this.edgeTo = edgeTo;
	}
	public double[] getDistTo() {
		return distTo;
	}
	public void setDistTo(double[] distTo) {
		this.distTo = distTo;
	}
	public BinaryHeapPQ<Integer, Double> getPq() {
		return pq;
	}
	public void setPq(BinaryHeapPQ<Integer, Double> pq) {
		this.pq = pq;
	}
	
}
