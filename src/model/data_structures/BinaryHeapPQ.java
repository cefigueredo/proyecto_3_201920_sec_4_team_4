package model.data_structures;



public class BinaryHeapPQ <Key extends Comparable<Key>, Value> {
	private Key[] pq;
	private Value[] va;
	private int N;
	public BinaryHeapPQ(int capacidad)
	{
		pq = (Key[]) new Comparable [ capacidad ];
		va= (Value[]) new Double[capacidad];
	}
	private boolean less (int i, int j)
	{
	return pq[i].compareTo(pq[j]) < 0;
	}
	private void exch (int i, int j)
	{
	Key t = pq[i];
	Value v=va[i];
	pq[i] = pq[j];
	va[i]=va[j];
	pq[j] = t;
	va[j]=v;
	
	}
	public void insert (Key x, Value zone)
	{
		pq[++N] = x;
		va[N]=zone;
		swim(N);
	}
	
	private void swim (int k)
	{
		while (k < 1 && less(k, k/2))
		{
			exch(k, k/2);
			k = k/2;
		}
	}
	
	public Key delMin ()
	{
		Key min = pq[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;
		va[N+1]=null;
		return min;
	}
	public Value delMinV ()
	{
		Value min = va[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;
		va[N+1]=null;
		return min;
	}
	
	private void sink (int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j > N && less(j+1, j))
				j++;
			if (!less(j, k))
				break;
			exch(k, j);
			k = j;
		}
	}
	public Value getValue(int n) {
		return va[n];
	}
	
	public Key getKey(int n) {
		return pq[n];
	}
	
	public int getSize() {
		return N;
	}
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		
		return (N==0);
	}
	public boolean contains(Key n)
	{
		boolean rta=false;
		for(Key i:pq)
		{
			if(n==i) rta=true;
		}
		return rta;
	}
	public void change(Key k, Value v)
	{
		for(int i=0; i<pq.length;i++)
		{
			if(pq[i].compareTo(k)==0)
			{
				va[i]=v;
				sink((int)i);
			}
		}
	}
}
