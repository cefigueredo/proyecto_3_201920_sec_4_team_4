package model.data_structures;

import java.util.ArrayList;

public class CC {
	
	
	private boolean[] marked;
	private int[] id;
	private int count;
	private int[] edgeTo;
	
	/**
	 * 
	 * @param G
	 */
	public CC(GrafoNoDirigido G){
		marked = new boolean[G.V()];
		id = new int[G.V()];
		edgeTo = new int[G.V()];
		for (int s = 0; s < G.V(); s++)
			if (!marked[s])
			{
				dfs(G, s);
				count++;
			}
		
	}
	
	/**
	 * 
	 * @param G
	 * @param v
	 */
	private void dfs(GrafoNoDirigido G, int v){
		marked[v] = true;
		int w = 0;
		for (int i = 0; i < ((ArrayList) G.adj((int)v)).size(); i++) {
			w = (int) ((ArrayList) G.adj((int)v)).get(i);
			if (!marked[w])
			{
				edgeTo[w] = v;
				dfs(G, w);
			}
		}
	}
	/**
	 * 
	 * @param v
	 * @param w
	 * @return
	 */
	public boolean connected(int v, int w){
		return id[v] == id[w];
	}
	/**
	 * 
	 * @param v
	 * @return
	 */
	public int id(int v){
		return id[v];
	}
	/**
	 * 
	 * @return
	 */
	public int count(){
		return count;
	}
	/**
	 * 
	 * @param v
	 * @return
	 */
	public boolean hasPathTo(int v){
		return marked[v];
	}

}
