package model.data_structures;

import java.util.Iterator;

public class Vertex <V, K> {
	private int id;
	private V info;
	public Vertex(int v, V info)
	{
		this.id=(int) v;
		this.info=info;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public V getInfo() {
		return info;
	}
	public void setInfo(V info) {
		this.info = info;
	}
	public String toString()
	{
		return " Latitud: "+((String) info).split(",")[1]+" Longitud: "+((String) info).split(",")[0];
	}
	
	
}
