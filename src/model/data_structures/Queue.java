package model.data_structures;

public class Queue<T> {
	private Node first;
	private Node last;
	private int N;
	public Queue()
	{
		first=null;
		last=null;
		N=0;
	}
	public void enqueue(T elem)
	{
		if(N==0)
		{
			first=(Node) elem;
			last=(Node) elem;
			N++;
		}
		else
		{
			last.next=(Node) elem;
			last=(Node) elem;
			last.next=null;
			N++;
		}
	}
	public T dequeue()
	{
		Node rt=first;
		first=first.next;
		N--;
		return (T) rt;
	}
	public int N() {
		// TODO Auto-generated method stub
		return N;
	}
}
