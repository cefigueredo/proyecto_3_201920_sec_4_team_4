package model.logic;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;

import com.google.gson.*;

import model.data_structures.Bag;
import model.data_structures.BinaryHeapPQ;
import model.data_structures.CC;
import model.data_structures.Edge;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.SP;
import model.data_structures.Vertex;

/**
 * Definicion del modelo del mundo
 * @param <K>
 * @param <V>
 *
 */
public class MVCModelo<K, V> {
	/**
	 * Atributos del modelo del mundo
	 */
	private GrafoNoDirigido<K, V> graph;
	private int arc;
	int cont;
	private UBERTripDay trip;
	private BinaryHeapPQ pq;
	private Queue mst;
	private boolean[] marked;
	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	@SuppressWarnings("unchecked")
	public MVCModelo()
	{
		graph=new GrafoNoDirigido();
	}

	public void cargarDatos() throws Exception
	{
		System.out.println("Cargando datos...");
		long begin=System.currentTimeMillis();
		String read=null;
		BufferedReader reader=new BufferedReader( new FileReader(new File("./data/bogota_vertices.txt")));
		reader.readLine();
		String[] data = new String[4];
		int cont=0;
		while((read=reader.readLine())!=null)
		{
			data=read.split(";");
			String info=data[1]+","+data[2]+","+data[3];
			graph.addVertex(Integer.parseInt(data[0]),info ); 
			cont++;	
			if(cont == 100)break;
			
		}
		System.out.println("Se cargaron "+cont+" vertices.");
		reader=new BufferedReader(new FileReader("./data/bogota_arcos.txt"));
		cont=0;
		while((read=reader.readLine())!=null)
		{
			data = read.split(" ");
			for(int i=1;i<data.length;i++) 
			{
				if(graph.getInfoVertex(Integer.parseInt( data[0])) != null &&  graph.getInfoVertex(Integer.parseInt( data[i]))!=null)
				{
					double dist=Haversine.distance(Double.parseDouble(( (String) graph.getInfoVertex(Integer.parseInt( data[0]))).split(",")[1])
							, Double.parseDouble(((String) graph.getInfoVertex(Integer.parseInt( data[0]))).split(",")[0])
							, Double.parseDouble(((String) graph.getInfoVertex(Integer.parseInt( data[i]))).split(",")[1])
							,Double.parseDouble( ((String) graph.getInfoVertex(Integer.parseInt(data[i]))).split(",")[0]));
					double time = 10;
					double vel = dist/time;
					graph.addEdge(Integer.parseInt(data[0]), Integer.parseInt(data[i]), dist, time, vel); 
					cont++;
					if(cont == 100)break;
					if(cont%10000==0) System.out.println("Cargando, por favor espere.");
				}
			}
			
		}
		arc = cont;
		
		System.out.println("Se cargaron "+arc+" arcos.");
		CC aux=new CC(graph); 
		System.out.println("Hay "+aux.count()+" componentes conectados en el grafo.");
		System.out.println("Tiempo requerido: "+(System.currentTimeMillis()-begin)+" ms");
	}
	public void generarJSON() throws Exception
	{
		PrintWriter pw= new PrintWriter(new FileWriter("./data/persistencia.json"));
		JsonArray array= new JsonArray();
		for(int i=0; i<graph.V();i++)
		{
			JsonObject obj=new JsonObject();
			obj.addProperty("InternalID", ((Vertex) graph.getVertices().get(i)).getId());
			obj.addProperty("ArrayID",i);
			obj.addProperty("Longitud",graph.getInfoVertex(((Vertex) graph.getVertices().get(i)).getId()).split(",")[1]);
			obj.addProperty("Latitud",graph.getInfoVertex(((Vertex) graph.getVertices().get(i)).getId()).split(",")[0]);
			String aux="";
			for(int j=0;j<((ArrayList) graph.adj(((Vertex) graph.getVertices().get(i)).getId())).size();j++)
			{
				aux+=((ArrayList) graph.adj(((Vertex) graph.getVertices().get(i)).getId())).get(j)+" ";
			}
			obj.addProperty("adj", aux);
			array.add(obj);
		}
		String json=array.toString();
		pw.println(json);
		pw.println("]");
	}

	@SuppressWarnings("deprecation")
	public void cargarGrafoPersistido() throws Exception {
		
		int nvert = 0; int narc = 0;
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(new FileReader("./data/persistencia.json"));
		JsonArray tamVert = (JsonArray)obj.get("Longitud").getAsJsonArray();
		nvert = tamVert.size();
		JsonArray tamAdj = (JsonArray)obj.get("adj").getAsJsonArray();
		narc = tamAdj.size(); narc = arc;
		
		System.out.println("Número de vertices: "+nvert+"\nNúmero de arcos: "+narc);	
	}
	
	public void darCantidadCC() {
		int totalCC = 0;
		graph.cc();
		totalCC = graph.id();
		System.out.println("Cantidad total de componentes conectados: "+totalCC);
	}
	
	
	
	//Parte 1A
	public void encontrarCaminoCostoMinimo(double latOr, double lngOr, double latDst, double lngDst)
	{
		int or=graph.buscarVerticeLocalización(latOr, lngOr);
		int dst=graph.buscarVerticeLocalización(latDst, lngDst);
		SP aux=new SP(graph, or);
		buildPath(aux,dst,or);
	}
	private void buildPath(SP sh,int dst,int st)
	{
		int act=dst;
		ArrayList path=new ArrayList();
		int conVer=0;
		double sumTime=0;
		double sumDist=0;
		sumTime=sh.getDistTo()[dst];
		while(act!=st)
		{
			path.add(sh.getEdgeTo()[act]);
			act=sh.getEdgeTo()[act];
			conVer++;
			sumDist+=graph.getCostArc(((Vertex) graph.getVertices().get(st)).getId(),((Vertex) graph.getVertices().get(dst)).getId())[0];
		}
		path.add(st);
		for(int i=path.size()-1;i>=0;i--)
		{
			System.out.println(path.get(i)+"\t"+graph.getVertices().get((int) path.get(i)).toString());
		}
	}
	//Parte 2A
	public void verticesMenorVel(int n)
	{
		GrafoNoDirigido gr=new GrafoNoDirigido(n);
		
		ArrayList aux=graph.darVerticesMenosVelocidad(n);
		gr.setVertices(aux);
		for(int i=0; i<aux.size();i++)
		{
			System.out.println(((Vertex) aux.get(i)).getId()+((Vertex) aux.get(i)).toString());
			Node auxB=((Bag) graph.adj(((Vertex) aux.get(i)).getId())).first();
			gr.addEdge(((Vertex) aux.get(i)).getId(), auxB.item(), auxB.value[0], auxB.value[1], auxB.value[2]);
			while(auxB.next!=null)
			{
				auxB=auxB.next;
				gr.addEdge(((Vertex) aux.get(i)).getId(),auxB.item(), auxB.value[0], auxB.value[1], auxB.value[2]);
			}
			
		}
		CC comp= new CC (gr);
		System.out.println("Hay "+comp.count()+" componentes conectados entre estos nodos.");
		for(int i=0; i<gr.V();i++)
		{
			System.out.println("El nodo de Id: "+((Vertex) gr.getVertices().get(i)).getId()+" hace parte del CC: "+comp.id(i));
		}
		
	}
	//Parte 3A
	public void MSTPrim()
	{
		double costotot=0;
		long time=System.currentTimeMillis();
		cont=0;
		int masrep=0;
		int idMas=0;
		CC comp=new CC (graph);
		int[] count=new int[graph.V()/10];
		Arrays.fill(count, 0);
		for(int i=0; i<graph.V()/10;i++)
		{
			count[comp.id(i)]++;
			
		}
		for(int i=0;i<count.length;i++)
		{
			if(count[i]>masrep) {masrep=count[i];
			idMas=i;
			}
		}
		for(int i=0;i<graph.V();i++)
		{
			if(comp.id(i)==idMas) System.out.print(((Vertex) graph.getVertices().get(i)).getId()+"\t");
		}
		pq=new BinaryHeapPQ(masrep*4);
		mst=new Queue();
		boolean[] marked=new boolean[graph.V()];
		int idlast=100000;
		int l=0;
		while(idlast!=idMas)
		{
			idlast=comp.id(l);
			l++;
		}
		visit(graph,l);
		while(!pq.isEmpty() && mst.N()<masrep-1)
		{
			Edge e=(Edge) pq.delMinV();
			int v=e.getV1().getId(); int w=e.getV2().getId();
			for(int i=0; i<graph.V();i++)
			{
				if(((Vertex) graph.getVertices().get(i)).getId()==v) v=i;
				if(((Vertex) graph.getVertices().get(i)).getId()==w) w=i;
			}
			if(marked[v]&&marked[w]) continue;
			mst.enqueue(e);
			costotot+=e.dist();
			if(!marked[v]) visit(graph,v);
			if(!marked[w]) visit(graph,w);
		}
		time=System.currentTimeMillis()-time;
		System.out.println("\nEl tiempo requerido fue de "+time+"ms");
		while(mst.N()!=0)
		{
			Edge x=(Edge) mst.dequeue();
			System.out.println(x.getV1()+"-"+x.getV2());
		}
		System.out.println("Hay "+cont+" nodos en el arbol");
		System.out.println("El costo total es: "+costotot);
	}
	private void visit(GrafoNoDirigido G, int v)
	{
		marked[v]=true;
		cont++;
		Node aux= ((Bag) G.adj(v)).first();
		while(aux!=null)
		{
			if(!marked[aux.item()])
				pq.insert(aux.value[0], new Edge((Vertex) G.getVertices().get(v), (Vertex)G.getVertices().get(aux.item()),aux.value[0]));
			aux=aux.next;
		}
	}
	
	
	//Parte 1B
	
	public void encontrarMenorHaversine(double latOr, double lngOr, double latDst, double lngDst) {
		int or = graph.buscarVerticeLocalización(latOr, lngOr);
		int dst = graph.buscarVerticeLocalización(latDst, lngDst);
		int aux = 0;
		for(int i = 0; i < graph.V(); i++) {
			int m = graph.darMenorHaversineEntreVertices(or, dst);
			System.out.println("Entre "+or+" y "+dst+" la distancia es:"+m);
			aux = or;
			or = dst;
			dst = aux;
		}
		

	}
	//Parte 2B
	
	public void darVerticesAlcanzables(double lat, double lng, int t) {
		int or = graph.buscarVerticeLocalización(lat, lng);
		int aux = 0;
		int[] vertex = new int[graph.V()];
		for(int i = 0; i < graph.V(); i++)
		{
			for(K m : graph.adj(or)) {
				Object dst = m;
				int menor = graph.darMenorHaversineEntreVertices(or, (int)dst);
				aux = or;
				or = (int)dst;
				double time = graph.getCostArc(or, (int)dst)[1];
				if(time < t)vertex[i] = (int)dst;
			}
			
		}
		for(int i = 0; i < vertex.length; i++) {
			System.out.println("Id: "+graph.getInfoVertex(vertex[i])+" en: "+t+" segundos.");
		}
	}
	//Parte 3B
	
	public void calcularMSTConKruskal() {
		double costotot=0;
		long time=System.currentTimeMillis();
		cont=0;
		int masrep=0;
		int idMas=0;
		CC comp=new CC (graph);
		int[] count=new int[graph.V()/10];
		Arrays.fill(count, 0);
		for(int i=0; i<graph.V()/10;i++)
		{
			count[comp.id(i)]++;
			
		}
		for(int i=0;i<count.length;i++)
		{
			if(count[i]>masrep) {masrep=count[i];
			idMas=i;
			}
		}
		for(int i=0;i<graph.V();i++)
		{
			if(comp.id(i)==idMas) System.out.print(((Vertex) graph.getVertices().get(i)).getId()+"\t");
		}
		pq=new BinaryHeapPQ(masrep*4);
		mst=new Queue();
		boolean[] marked=new boolean[graph.V()];
		int idlast=100000;
		int l=0;
		while(idlast!=idMas)
		{
			idlast=comp.id(l);
			l++;
		}
		visit(graph,l);
		while(!pq.isEmpty() && mst.N()<masrep-1)
		{
			Edge e=(Edge) pq.delMinV();
			int v=e.getV1().getId(); int w=e.getV2().getId();
			for(int i=0; i<graph.V();i++)
			{
				if(((Vertex) graph.getVertices().get(i)).getId()==v) v=i;
				if(((Vertex) graph.getVertices().get(i)).getId()==w) w=i;
			}
			if(marked[v]&&marked[w]) continue;
			mst.enqueue(e);
			costotot+=e.dist();
			if(!marked[v]) visit(graph,v);
			if(!marked[w]) visit(graph,w);
		}
		time=System.currentTimeMillis()-time;
		System.out.println("\nEl tiempo requerido fue de "+time+"ms");
		while(mst.N()!=0)
		{
			Edge x=(Edge) mst.dequeue();
			System.out.println(x.getV1()+"-"+x.getV2());
		}
		System.out.println("Hay "+cont+" nodos en el arbol");
		System.out.println("El costo total es: "+costotot);
	}
	
	//Parte 1C
	public void construirGrafoSimplificado() {
		
	}
	
	//Parte 2C
	public void calcularCaminoCostoMinimo() {
		
	}
	
	//Parte 3C
	public void calcularMenorLongitudZonaAlcanzable() {
		
	}
}
